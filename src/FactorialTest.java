import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by tkanc on 8/23/2017.
 */
public class FactorialTest {
    @Test
    public void factorial() throws Exception {
        for(int i=0; i<=10; i++) {
            assertEquals("Expected " +Factorial.factorial(i), Factorial.factorial(i), Factorial.iterationFactorial(i));
        }
    }

}
